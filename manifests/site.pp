Package {
	require => File['/etc/apt/apt.conf.d/local-recommends']
}

File {
	owner  => root,
	group  => root,
	mode   => '0444',
	ensure => file,
}

Exec {
	path => '/usr/bin:/usr/sbin:/bin:/sbin'
}

Service {
	hasrestart => true,
	hasstatus  => true,
}

node default {
	# we really should rename this one
	include deprecated
	include base

	# this is magic: it will include whatever classes says we should
	# include, based on the value of the "classes" array
	hiera_include('classes')

	include roles::pubsub::client
	class { 'roles::udldap::client':
		ensure => absent
	}

	if getfromhash($deprecated::nodeinfo, 'ganeti') {
		include ganeti2
	}

	if $::kernel == 'Linux' {
		include linux
		include acpi
	}

	if $::brokenhosts {
		include hosts
	}

	if $::samhain {
		include samhain
	}

	if $::spamd {
		include spamassassin
	}

	if $::apache2 {
		if ! defined(Class['apache2']) {
			fail('We have apache installed but no role pulled it in.')
		}
	}
}
