# our mailrelay exim class
class exim::mailrelay {
  class { 'exim::mx':
    is_mailrelay  => true,
  }

  concat::fragment { 'puppet-crontab--email-virtualdomains':
    target  => '/etc/cron.d/puppet-crontab',
    content => @(EOF)
      @hourly  root if [ ! -d /etc/exim4/email-virtualdomains ]; then cd /etc/exim4 && git clone mail-git:email-virtualdomains ; fi && cd /etc/exim4/email-virtualdomains && git pull --quiet --ff-only
      | EOF
  }
  concat::fragment { 'virtual_domain_template':
    target  => '/etc/exim4/virtualdomains',
    content => template('exim/virtualdomains-mailrelay.erb'),
  }

  concat { '/etc/exim4/manualroute':
    mode           => '0444',
    ensure_newline => true,
    warn           => '# This file is maintained with puppet',
    require        => Package['exim4-daemon-heavy']
  }
  Concat::Fragment <<| tag == 'exim::manualroute::to::mailrelay' |>>
}
