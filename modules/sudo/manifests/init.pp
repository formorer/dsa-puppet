# Debian.org's sudo setup
class sudo {
  ensure_packages ( [
   'sudo',
   'libpam-pwdfile',
  ], {
     ensure => 'installed',
  })

  file { '/etc/pam.d/sudo':
    source  => 'puppet:///modules/sudo/pam',
    require => Package['sudo'],
  }

  file { '/etc/sudoers':
    mode    => '0440',
    source  => 'puppet:///modules/sudo/sudoers',
    require => Package['sudo'],
  }

  file { '/etc/sudoers.d':
    ensure  => directory,
    mode    => '755',
    purge   => true,
    recurse => true,
    force   => true,
    source  => 'puppet:///files/empty/',
  }
  file { '/etc/sudoers.d/README':
    mode    => '440',
    content => @(EOT),
      # According to the README shipped with Debian 10,
      # this directory, if included in /etc/sudoers, needs
      # to contain at least one file.  Files which end in
      # a '~' character or that contain a '.' are ignored.
      #
      # Files should be mode 0440 and be edited with visudo.
      | EOT
  }
}
