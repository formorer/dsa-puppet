# /etc/sudoers
##
## THIS FILE IS UNDER PUPPET CONTROL. DON'T EDIT IT HERE.
## USE: git clone git+ssh://$USER@puppet.debian.org/srv/puppet.debian.org/git/dsa-puppet.git
##

###################################################################
###################################################################
###################################################################
##
## PLEASE EDIT THIS FILE WITH THE visudo COMMAND TO ENSURE IT
## IS SYNTACTICALLY VALID.
##
##  /usr/sbin/visudo -f sudoers
##
###################################################################
###################################################################
###################################################################

Defaults	env_reset
Defaults	passprompt="[sudo] password for %u on %h: "
Defaults        secure_path="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

# Find binaries to be executed as archvsync user also in its home, so the
# caller does not need to know.
Defaults>archvsync secure_path="/home/archvsync/bin:/usr/local/bin:/usr/bin:/bin"

# Host alias specification
Host_Alias	VOIPHOSTS	= vogler
Host_Alias	WEBHOSTS	= wolkenstein
Host_Alias	SECHOSTS	= seger
Host_Alias	FTPHOSTS	= fasolo
Host_Alias	ZIVITHOSTS	= zelenka, zandonai
Host_Alias	AACRAIDHOSTS	= pettersson
Host_Alias	MEGARAIDHOSTS	= sibelius
Host_Alias	MEGARAIDSASHOSTS = grnet-node03, grnet-node04
Host_Alias	DELLHOSTS	= manda-node03, manda-node04, schmelzer, smit, klecker
Host_Alias	LISTHOSTS	= bendel
Host_Alias	BUILDD_MASTER	= wuiet
Host_Alias	PORTERBOXES	= abel, amdahl, barriere, eller, harris, minkus, plummer, zelenka
Host_Alias	PIUPARTS_SLAVE_HOSTS	= piu-slave-ubc-01, piu-slave-conova-01
Host_Alias	MQ_HOSTS	= rainier, rapoport

# Cmnd alias specification

# User privilege specification
root	ALL=(ALL) ALL


# DSA and local admins
%adm		ALL=(ALL)	ALL

%adm		ALL=(ALL)	NOPASSWD: /usr/bin/apt-get update, /usr/bin/apt-get upgrade, /usr/bin/apt-get dist-upgrade, /usr/bin/apt-get clean, /usr/sbin/samhain -t check -i -p err -s none -l none -m none, /usr/sbin/upgrade-porter-chroots

%zivit-admins	ZIVITHOSTS=(ALL)	NOPASSWD: ALL

# nagios
nagios		MQ_HOSTS=(rabbitmq)	NOPASSWD: /usr/sbin/rabbitmqctl list_queues -p dsa name messages consumers
nagios		ALL=(ALL)	NOPASSWD: /usr/sbin/service ekeyd-egd-linux restart
nagios		ALL=(ALL)	NOPASSWD: /usr/sbin/service samhain restart
nagios		ALL=(ALL)	NOPASSWD: /usr/lib/nagios/plugins/dsa-check-dabackup ""
nagios		ALL=(ALL)	NOPASSWD: /usr/lib/nagios/plugins/dsa-check-filesystems ""
nagios		ALL=(ALL)	NOPASSWD: /usr/lib/nagios/plugins/dsa-check-libs --ignore-younger=1h
nagios		ALL=(ALL)	NOPASSWD: /usr/lib/nagios/plugins/dsa-check-stunnel-sanity ""
nagios		ALL=(ALL)	NOPASSWD: /usr/lib/nagios/plugins/dsa-check-systemd-services ""
nagios		ALL=(ALL)	NOPASSWD: /usr/lib/nagios/plugins/dsa-check-ucode-intel ""
nagios		ALL=(ALL)	NOPASSWD: /usr/lib/nagios/plugins/dsa-check-cert-expire /var/lib/puppet/ssl/certs/*.pem
nagios		ALL=(ALL)	NOPASSWD: /usr/lib/nagios/plugins/dsa-check-cert-expire /etc/ntp.keys.d/ntpkey_cert_*
nagios		handel=(puppet)	NOPASSWD: /usr/lib/nagios/plugins/dsa-check-cert-expire /var/lib/puppet/ssl/certs/ca.pem
nagios		handel=(puppet)	NOPASSWD: /usr/lib/nagios/plugins/dsa-check-cert-expire /srv/puppet.debian.org/ca/ca.crt
nagios		handel=(ALL)	NOPASSWD: /usr/lib/nagios/plugins/dsa-check-puppet-cert-expire ""

# SMART monitoring for hard drives
nagios		ALL=(ALL)	NOPASSWD: /usr/sbin/smartctl -d auto -Hi /dev/sd[a-z]
nagios		ALL=(ALL)	NOPASSWD: /usr/sbin/smartctl -d auto -q silent -A /dev/sd[a-z]
nagios		ALL=(ALL)	NOPASSWD: /usr/sbin/smartctl -d auto -A /dev/sd[a-z]

# with smartarray controllers
nagios		ALL=(ALL)	NOPASSWD: /sbin/hpasmcli ""
nagios		ALL=(ALL)	NOPASSWD: /usr/bin/arrayprobe ""
nagios		ALL=(ALL)	NOPASSWD: /usr/sbin/hpssacli controller all show
nagios		ALL=(ALL)	NOPASSWD: /usr/sbin/hpssacli controller slot=[0129] ld all show
nagios		ALL=(ALL)	NOPASSWD: /usr/sbin/hpssacli controller slot=[0129] ld [0-9] show
nagios		ALL=(ALL)	NOPASSWD: /usr/sbin/hpssacli controller slot=[0129] pd all show
nagios		ALL=(ALL)	NOPASSWD: /usr/sbin/hpssacli controller slot=[0129] pd [0-9]\:[0-9] show
nagios		ALL=(ALL)	NOPASSWD: /usr/sbin/hpssacli controller slot=[0129] pd [0-9][EIC]\:[0-9]\:[0-9] show
nagios		ALL=(ALL)	NOPASSWD: /usr/sbin/hpssacli controller slot=[0129] pd [0-9][EIC]\:[0-9]\:[0-9][0-9] show
nagios		ALL=(ALL)	NOPASSWD: /usr/sbin/hpssacli controller slot=[0129] show status
nagios		ALL=(ALL)	NOPASSWD: /usr/sbin/hpacucli controller all show
nagios		ALL=(ALL)	NOPASSWD: /usr/sbin/hpacucli controller slot=[0129] ld all show
nagios		ALL=(ALL)	NOPASSWD: /usr/sbin/hpacucli controller slot=[0129] ld [0-9] show
nagios		ALL=(ALL)	NOPASSWD: /usr/sbin/hpacucli controller slot=[0129] pd all show
nagios		ALL=(ALL)	NOPASSWD: /usr/sbin/hpacucli controller slot=[0129] pd [0-9]\:[0-9] show
nagios		ALL=(ALL)	NOPASSWD: /usr/sbin/hpacucli controller slot=[0129] pd [0-9][EIC]\:[0-9]\:[0-9] show
nagios		ALL=(ALL)	NOPASSWD: /usr/sbin/hpacucli controller slot=[0129] pd [0-9][EIC]\:[0-9]\:[0-9][0-9] show
nagios		ALL=(ALL)	NOPASSWD: /usr/sbin/hpacucli controller slot=[0129] show status

# other raid controllers
nagios		AACRAIDHOSTS=(ALL)	NOPASSWD: /usr/local/bin/arcconf GETCONFIG 1 LD, /usr/local/bin/arcconf GETCONFIG 1 AD
nagios		MEGARAIDHOSTS=(ALL)	NOPASSWD: /usr/local/bin/megarc -AllAdpInfo -nolog, /usr/local/bin/megarc -dispCfg -a0 -nolog
nagios		MEGARAIDSASHOSTS=(ALL)	NOPASSWD: /usr/local/sbin/megacli -adpCount -NoLog
nagios		MEGARAIDSASHOSTS=(ALL)	NOPASSWD: /usr/local/sbin/megacli -PdList -a[0-9] -NoLog
nagios		MEGARAIDSASHOSTS=(ALL)	NOPASSWD: /usr/local/sbin/megacli -LdGetNum -a[0-9] -NoLog
nagios		MEGARAIDSASHOSTS=(ALL)	NOPASSWD: /usr/local/sbin/megacli -LdInfo -L[0-9] -a[0-9] -NoLog
nagios		DELLHOSTS=(ALL)		NOPASSWD: /usr/lib/nagios/plugins/dsa-check-openmanage ""
nagios		DELLHOSTS=(ALL)		NOPASSWD: /usr/lib/nagios/plugins/dsa-check-openmanage -b bp=0
nagios		DELLHOSTS=(ALL)		NOPASSWD: /usr/lib/nagios/plugins/dsa-check-openmanage -b bp=0 -b bat_charge=0\:0

# groups and their role accounts
%alioth-archive	ALL=(alioth-archive)	ALL
%appstream	ALL=(appstream)	ALL
%auditor	ALL=(accounting)	ALL
%backports	ALL=(backports)	ALL
%blends		ALL=(blends)	ALL
%bootstrap	ALL=(bootstrap)	ALL
%btslink	ALL=(btslink)	ALL
%buildd		ALL=(buildd)	ALL
%codesearch	ALL=(codesearch)	ALL
%community	ALL=(community)	ALL
%d-i		ALL=(d-i)	ALL
%dde		ALL=(dde)	ALL
%ddtp		ALL=(ddtp)	ALL
%debadmin	ALL=(dak)	ALL
%debadmin	ALL=(dak-code)	ALL
%debadmin	ALL=(codesign)	ALL
%debbugs	ALL=(debbugs)	ALL
%debbugs	ALL=(debbugs-mirror)	ALL
%debconfstatic	ALL=(debconfstatic)	ALL
%debdelta	ALL=(debdelta)	ALL
%debian-cd	ALL=(debian-cd)	ALL
%cloud-build	ALL=(cloud-build)	ALL
%cloud-ci	ALL=(cloud-ci)	ALL
%cloud-test	ALL=(cloud-test)	ALL
%cloud-deploy-aws	ALL=(cloud-deploy-aws)	ALL
%cloud-deploy-azure	ALL=(cloud-deploy-azure)	ALL
%cloud-deploy-gce	ALL=(cloud-deploy-gce)	ALL
%cloud-deploy-openstack	ALL=(cloud-deploy-openstack)	ALL
%cloud-deploy-digitalocean	ALL=(cloud-deploy-digitalocean)	ALL
%debian-i18n	ALL=(debian-i18n)	ALL
%debian-r	ALL=(debian-r)	ALL
%debian-r	ALL=(debian-r-wb-buildd)	ALL
%debian-release	ALL=(release)	ALL
%debsources	ALL=(debsources)	ALL
%debtags	ALL=(debtags)	ALL
%debwww		ALL=(debwww)	ALL
%dedup		ALL=(dedup)	ALL
%dgit		ALL=(dgit)	ALL
%dgit		ALL=(dgit-unpriv)	ALL
%emdebian	ALL=(emdebian)	ALL
%forums		ALL=(forums)	ALL
%gitdoadm	ALL=(gitdoadm)	ALL
%keyring	ALL=(keyring)	ALL
%keyring	kaufmann=(root)		NOPASSWD: /usr/sbin/service bind9 reload
%lintian	ALL=(lintian)	ALL
%lintian	ALL=(lintian-website)	ALL
%listweb	ALL=(listweb)	ALL
%list		LISTHOSTS=(list)	ALL
# archives and stuff
%list		master=(debian)	ALL
%manpages	ALL=(manpages)	ALL
%mirroradm	ALL=(archvsync)	ALL
%mirroradm	melartin=(mirroradm)	ALL
%nm		ALL=(nm)	ALL
%nm		ALL=(nm-web)	ALL
%contributors	ALL=(contributors)	ALL
%contributors	ALL=(contributors-web)	ALL
%patch-tracker	ALL=(patch-tracker)	ALL
%piuparts	ALL=(piupartsm)	ALL
%piuparts	ALL=(piupartss)	ALL
%pkg_maint	ALL=(pkg_user)	ALL
%planet		ALL=(planet)	ALL
%popcon		ALL=(popcon)	ALL
%ports		ALL=(ports)	ALL
%search		ALL=(search)	ALL
%secretary	ALL=(secretary)	ALL
%sectracker	ALL=(sectracker)	ALL
%security	SECHOSTS=(mail_security)	ALL
%security	ALL=(security)	ALL
%snapshot	ALL=(snapshot)	ALL
%uddadm		ALL=(udd)	ALL
%videoteam	ALL=(videoteam)	ALL
%videoteam	vittoria=(veyepar)	ALL
%videoteam	vittoria=(sreview)	ALL
%volatile	ALL=(volatile)	ALL
%wbadm		ALL=(wbadm)	ALL
%mujeres	ALL=(women)	ALL
%wikiadm	ALL=(wiki,wikiweb)	ALL
%qa-core	ALL=(qa)	ALL
%gobby		gombert=(gobby)	ALL
%dacshelper	diabelli=(www-data)	ALL
%debsso		diabelli=(debsso)	ALL
%debsso-web	diabelli=(debsso-web)	ALL
%debconf-web	debussy=(debconf-web)	ALL

# the release user gets to run stuff as release-unpriv (for things like lintian checks)
release		ALL=(release-unpriv)	NOPASSWD: ALL

# the dak user gets to run stuff as dak-unpriv (for things like lintian checks)
%ftptrainee	FTPHOSTS=(dak-unpriv)	NOPASSWD: /usr/bin/lintian
dak		ALL=(dak-unpriv)	NOPASSWD: ALL
# and ftpmaster can access the role user for their web services
%debadmin	FTPHOSTS=(dak-web)	ALL

# some groups are in apachectrl on "their" hosts so they can reload apache and update their vhost
%apachectrl	ALL=(root)	/usr/sbin/apache2-vhost-update

# buildd
# FIXME: change that ALL for hosts to a hostlist of buildds?
Defaults:buildd env_reset,env_keep+="APT_CONFIG DEBIAN_FRONTEND"
buildd		ALL=(ALL)		NOPASSWD: ALL

%appstream	mekeel=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component appstream.debian.org
%backports	FTPHOSTS,coccia=(staticsync)	NOPASSWD: /usr/local/bin/static-update-component backports.debian.org
%bootstrap	boott=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component bootstrap.debian.net
d-i		dillon=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component d-i.debian.org
debian-cd	casulana=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component cdbuilder-logs.debian.org
lucas		dillon=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component debaday.debian.net
dsa		dillon=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component dsa.debian.org
dak		FTPHOSTS=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component incoming.debian.org
dak		FTPHOSTS=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component metadata.ftp-master.debian.org
codesign	FTPHOSTS=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component metadata.ftp-master.debian.org
%publicity	dillon=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component bits.debian.org
%publicity	dillon=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component micronews.debian.org
%mirroradm	melartin=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component mirror-master.debian.org
%debdelta	donizetti=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component debdeltas.debian.net
%webwml		master=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component network-test.debian.org
planet		philp=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component planet.debian.org
debwww		wolkenstein=(staticsync)	NOPASSWD: /usr/local/bin/static-update-component www.debian.org
%blends		dillon=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component blends.debian.org
%Debian		dillon=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component wnpp-by-tags.debian.net
%Debian		dillon=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component mozilla.debian.net
%ports		dillon=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component www.ports.debian.org
%debian-release	respighi=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component release.debian.org
%debian-release	coccia=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component release.debian.org-pu
%security	dillon=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component security-team.debian.org
%publicity	dillon=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component timeline.debian.net
pabs		dillon=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component timeline.debian.net
%debconfstatic	dillon=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component 10years.debconf.org
%debconfstatic	dillon=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component debconf0.debconf.org
%debconfstatic	dillon=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component debconf1.debconf.org
%debconfstatic	dillon=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component debconf2.debconf.org
%debconfstatic	dillon=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component debconf3.debconf.org
%debconfstatic	dillon=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component debconf4.debconf.org
%debconfstatic	dillon=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component debconf5.debconf.org
%debconfstatic	dillon=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component debconf6.debconf.org
%debconfstatic	dillon=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component debconf7.debconf.org
%debconfstatic	dillon=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component debconf16.debconf.org
%debconfstatic	dillon=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component debconf17.debconf.org
%debconfstatic	dillon=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component debconf18.debconf.org
%debconfstatic	dillon=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component debconf19.debconf.org
%debconfstatic	dillon=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component debconf20.debconf.org
%debconfstatic	dillon=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component es.debconf.org
%debconfstatic	dillon=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component fr.debconf.org
%debconfstatic	dillon=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component miniconf10.debconf.org
%debconfstatic	dillon=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component wiki.debconf.org
%debconfstatic	dillon=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component www.debconf.org
%debconfstatic	dillon=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component media.debconf.org
mini-dak	porta=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component incoming.ports.debian.org
%manpages	manziarly=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component manpages.debian.org
%dpl		dillon=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component dpl.debian.org
%keyring	kaufmann=(staticsync)		NOPASSWD: /usr/local/bin/static-update-component openpgpkey.debian.org

# The piuparts slave needs to handle chroots
piupartss	PIUPARTS_SLAVE_HOSTS=(ALL)		NOPASSWD: ALL
# trigger of mirror run for packages
dnsadm		denis=(root)			NOPASSWD: /usr/sbin/service bind9 reload
letsencrypt	denis=(dnsadm)			NOPASSWD: /srv/dns.debian.org/bin/update
# wbadm can update all buildd* users' keys on buildd.d.o
%wbadm		BUILDD_MASTER=(wb-buildd)	ALL
%wbadm		BUILDD_MASTER=(root)		/usr/local/bin/update-buildd-sshkeys
# wbadm can impersonate the wbadm-web user
%wbadm		BUILDD_MASTER=(wbadm-web)	ALL
# mirror push
dak		FTPHOSTS,SECHOSTS=(archvsync)	NOPASSWD:/home/archvsync/runmirrors, /home/archvsync/bin/runmirrors
# archvsync triggers snapshot
archvsync	sibelius,sallinen=(snapshot)	NOPASSWD: /srv/snapshot.debian.org/bin/update-trigger
archvsync	sibelius=(snapshot)		NOPASSWD: /srv/2ndsnapshot/bin/update-trigger
# dak stuff
%debian-release	FTPHOSTS=(dak)		/usr/local/bin/dak transitions --import *
%ftpteam	FTPHOSTS=(dak)		/usr/local/bin/dak transitions --import *
# security
%security	SECHOSTS=(dak)		NOPASSWD: /usr/local/bin/dak new-security-install -[AR]
%sec_public	SECHOSTS=(dak)		NOPASSWD: /usr/local/bin/dak new-security-install -[AR]
%sec_public	SECHOSTS=(dak)		NOPASSWD: /home/dak/trigger_mirror
dak		SECHOSTS=(archvsync)	NOPASSWD: /home/archvsync/signal_security
dak		SECHOSTS=(archvsync)	NOPASSWD: /home/archvsync/signal_security-buildd-pool
# web stuff
debwww		WEBHOSTS=(archvsync)	NOPASSWD: /home/archvsync/webmirrors/runmirrors
%publicity	WEBHOSTS=(debwww)	/srv/www.debian.org/update-part News
%debvote	WEBHOSTS=(debwww)	/srv/www.debian.org/update-part vote
%d-i		WEBHOSTS=(debwww)	/srv/www.debian.org/update-part devel/debian-installer
%d-i		WEBHOSTS=(debwww)	/srv/www.debian.org/cron/lessoften-parts/1installation-guide
# more list stuff
%list		LISTHOSTS=(postfix)		/usr/sbin/postcat
%list		LISTHOSTS=(root)		/usr/sbin/postfix reload
%list		LISTHOSTS=(root)		/usr/sbin/qshape, /usr/sbin/postsuper
%list		LISTHOSTS=(root)		/usr/sbin/service spamassassin restart, /usr/sbin/service spamassassin reload, /usr/sbin/service spamassassin stop, /usr/sbin/service spamassassin start
%list		LISTHOSTS=(root)		/usr/sbin/service amavis restart, /usr/sbin/service amavis reload, /usr/sbin/service amavis stop, /usr/sbin/service amavis start
%list		LISTHOSTS=(amavis)		NOPASSWD: /usr/bin/sa-learn
%list		LISTHOSTS=(amavis)		ALL
# geodns may reload bind
geodnssync	geo1,geo2,geo3=(root)	NOPASSWD: /usr/sbin/service bind9 reload
geodnssync	geo1,geo2,geo3=(root)	NOPASSWD: /usr/sbin/rndc reconfig
# pushed nagiosadm reload icinga on tchaikovsky
nagiosadm	tchaikovsky=(root)		NOPASSWD: /usr/sbin/service icinga reload
# voip stuff
%debvoip	VOIPHOSTS=(root)	/usr/sbin/service prosody restart, /usr/sbin/service prosody reload, /usr/sbin/service prosody stop, /usr/sbin/service prosody start
# snapshot can reload apache to get the wsgi reloaded
snapshot	lw07,sallinen=(root)	NOPASSWD: /usr/sbin/service apache2 reload

%Debian,%guest,%d-i	PORTERBOXES=(root)	NOPASSWD: /usr/local/bin/dd-schroot-cmd


# ports stuff
mini-dak	porta=(archvsync)	NOPASSWD: /home/archvsync/signal_ports

#includedir /etc/sudoers.d
