# Create an (empty) chain
#
# @param domain netfilter domain: ip (IPv4), ip6 (IPv6), or both.
# @param table  netfilter table
# @param chain  netfilter chain
# @param description a description of the rule
# @param prio   Priority/Order of the rule
define ferm::rule::chain (
  String $chain,
  String $description = '',
  Variant[Enum['ip', 'ip6'], Array[Enum['ip', 'ip6']]] $domain = ['ip', 'ip6'],
  String $table = 'filter',
  String $prio = '10',
) {
  include ferm

  $real_domain = Array($domain, true)

  file {
    "/etc/ferm/dsa.d/${prio}_${name}":
      ensure  => 'present',
      mode    => '0400',
      notify  => Exec['ferm reload'],
      content => inline_template( @(EOF) ),
                    domain (<%= @real_domain.join(' ') %>) {
                      table <%= @table %> {
                        chain <%= @chain %> {}
                      }
                    }
                    | EOF
  }
}
