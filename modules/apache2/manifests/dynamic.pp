class apache2::dynamic {
  ferm::rule { 'dsa-http-limit':
    prio        => '20',
    description => 'limit HTTP DOS',
    chain       => 'http_limit',
    domain      => '(ip ip6)',
    rule        => 'mod limit limit-burst 60 limit 15/minute jump ACCEPT;
                    jump DROP'
  }

  ferm::rule { 'dsa-http-soso':
    prio        => '21',
    description => 'slow soso spider',
    chain       => 'limit_sosospider',
    domain      => '(ip ip6)',
    rule        => 'mod connlimit connlimit-above 2 connlimit-mask 21 jump DROP;
                    jump http_limit'
  }

  ferm::rule { 'dsa-http-yahoo':
    prio        => '21',
    description => 'slow yahoo spider',
    chain       => 'limit_yahoo',
    domain      => '(ip ip6)',
    rule        => 'mod connlimit connlimit-above 2 connlimit-mask 16 jump DROP;
                    jump http_limit'
  }

  ferm::rule { 'dsa-http-google':
    prio        => '21',
    description => 'slow google spider',
    chain       => 'limit_google',
    domain      => '(ip ip6)',
    rule        => 'mod connlimit connlimit-above 2 connlimit-mask 19 jump DROP;
                    jump http_limit'
  }

  ferm::rule { 'dsa-http-bing':
    prio        => '21',
    description => 'slow bing spider',
    chain       => 'limit_bing',
    domain      => '(ip ip6)',
    rule        => 'mod connlimit connlimit-above 2 connlimit-mask 16 jump DROP;
                    jump http_limit'
  }

  ferm::rule { 'dsa-http-baidu':
    prio        => '21',
    description => 'slow baidu spider',
    chain       => 'limit_baidu',
    domain      => '(ip ip6)',
    rule        => 'mod connlimit connlimit-above 2 connlimit-mask 16 jump DROP;
                    jump http_limit'
  }
  ferm::rule { 'dsa-http-nhn':
    prio        => '21',
    description => 'slow nhn spider',
    chain       => 'limit_nhn',
    domain      => '(ip ip6)',
    rule        => 'mod connlimit connlimit-above 2 connlimit-mask 16 jump DROP;
                    jump http_limit'
  }
  ferm::rule { 'dsa-http-apple':
    prio        => '21',
    description => 'slow apple spider',
    chain       => 'limit_applebot',
    domain      => '(ip ip6)',
    rule        => 'mod connlimit connlimit-above 2 connlimit-mask 16 jump DROP;
                    jump http_limit'
  }
  ferm::rule { 'dsa-http-petalbot':
    prio        => '21',
    description => 'slow petalbot / AspiegelBot spider',
    chain       => 'limit_petalbot',
    domain      => '(ip ip6)',
    rule        => 'mod connlimit connlimit-above 2 connlimit-mask 18 jump DROP;
                    jump http_limit'
  }

  ferm::rule { 'dsa-http-rules':
    prio        => '22',
    description => 'http subchain',
    chain       => 'http',
    domain      => '(ip ip6)',
    rule        => '
        saddr (74.6.22.182 74.6.18.240 67.195.0.0/16) jump limit_yahoo;
        saddr (124.115.0.0/21 119.63.192.0/21) jump limit_sosospider;
        saddr (65.52.0.0/14 207.46.0.0/16 157.54.0.0/15 40.76.0.0/14) jump limit_bing;
        saddr (66.249.64.0/19) jump limit_google;
        saddr (123.125.71.0/24 119.63.192.0/21 180.76.0.0/16 220.181.0.0/16) jump limit_baidu;
        saddr (119.235.237.0/24) jump limit_nhn;
        saddr (17.58.0.0/16) jump limit_applebot;
        saddr (114.119.128.0/18) jump limit_petalbot;

        mod recent name HTTPDOS update seconds 1800 jump log_or_drop;
        mod hashlimit hashlimit-name HTTPDOS hashlimit-mode srcip hashlimit-burst 600 hashlimit 30/minute jump ACCEPT;
        mod recent name HTTPDOS set jump log_or_drop'
  }
}
