# vim: set ts=2 sw=2 et si sm:


<Macro docker-registry-common $hostname $innerblock>
  Use common-debian-service-https-redirect * $hostname

  <VirtualHost *:443>
    ServerName $hostname
    ServerAdmin debian-admin@lists.debian.org

    Use common-debian-service-ssl $hostname
    Use common-ssl-HSTS

    <IfModule mod_userdir.c>
      UserDir disabled
    </IfModule>

    ErrorLog /var/log/apache2/$hostname-error.log
    CustomLog /var/log/apache2/$hostname-access.log combined
    #CustomLog /var/log/apache2/docker-registry.debian.org-access.log privacy
    ServerSignature On

    <IfModule mod_proxy.c>
      ProxyRequests     Off
      ProxyPreserveHost on
      ProxyPass        /v2 http://127.0.0.1:5000/v2
      ProxyPassReverse /v2 http://127.0.0.1:5000/v2
      RequestHeader set X-Forwarded-Proto "https"

      <Location /v2>
        Use $innerblock
      </Location>
    </IfModule>
  </VirtualHost>
</Macro>

<Macro docker-registry-public>
  <LimitExcept GET HEAD>
    Require all denied
  </LimitExcept>
</Macro>

<Macro docker-registry-privileged>
  AuthName "debian.org docker-registry"
  AuthType Basic
  AuthDigestProvider file
  AuthUserFile /etc/apache2/docker-registry-priv.debian.org.htpasswd

  Require valid-user
</Macro>

Use docker-registry-common docker-registry.debian.org docker-registry-public
Use docker-registry-common docker-registry-priv.debian.org docker-registry-privileged

# vim: set filetype=apache:
