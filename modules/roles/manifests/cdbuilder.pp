# basic bits for debian-cd
class roles::cdbuilder {
  # debian-cd wants to make hardlinks to files it doesn't own; let it.
  file { '/etc/sysctl.d/protect-links.conf':
    ensure => absent,
  }
  base::sysctl { 'cdimage-software-really-needs-to-be-fixed':
    key   => 'fs.protected_hardlinks',
    value => '0',
  }

  file { '/srv/cdbuilder.debian.org':
    ensure => directory,
    owner  => 'debian-cd',
    group  => 'debian-cd',
    mode   => '2775'
  }
  file { '/home/debian-cd':
    ensure => link,
    target => '/srv/cdbuilder.debian.org',
  }
}
