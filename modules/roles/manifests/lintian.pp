# lintian.debian.org role
#
# @param db_address     hostname of the postgres server for this service
# @param db_port        port of the postgres server for this service
class roles::lintian (
  String  $db_address,
  Integer $db_port,
) {
  include apache2
  include apache2::ssl
  include apache2::proxy_http

  ssl::service { 'lintian.debian.org': notify  => Exec['service apache2 reload'], key => true, }
  apache2::site { 'lintian.debian.org':
    site    => 'lintian.debian.org',
    content => template('roles/apache-lintian.debian.org.conf.erb'),
  }
  onion::service { 'lintian.debian.org': port => 80, target_address => 'lintian.debian.org', target_port => 80, direct => true }

  $srv_dir = '/srv/lintian.debian.org';
  file { $srv_dir:
    ensure => directory,
    owner  => 'lintian',
    group  => 'lintian',
    mode   => '2755'
  }
  file { "${srv_dir}/www":
    ensure => directory,
    owner  => 'lintian',
    group  => 'lintian',
    mode   => '2755'
  }

  file { "${srv_dir}/home":
    ensure => directory,
    owner  => 'lintian',
    group  => 'lintian',
    mode   => '2755'
  }
  file { '/home/lintian':
    ensure => link,
    target => "${srv_dir}/home"
  }
  file { "${srv_dir}/home-www":
    ensure => directory,
    owner  => 'lintian-website',
    group  => 'lintian-website',
    mode   => '2755'
  }
  file { '/home/lintian-website':
    ensure => link,
    target => "${srv_dir}/home-www"
  }


  @@postgres::cluster::hba_entry { "lintian-${::fqdn}":
    tag      => "postgres::cluster::${db_port}::hba::${db_address}",
    pg_port  => $db_port,
    database => ['lintian'],
    user     => ['lintian', 'lintian-website'],
    address  => $base::public_addresses,
  }

  dsa_systemd::linger { 'lintian': }
  dsa_systemd::linger { 'lintian-website': }
}
