# a static mirror.  It will get content for all components that it servers
# from each component's master.
class roles::static_mirror () {
  include staticsync::static_mirror
}
