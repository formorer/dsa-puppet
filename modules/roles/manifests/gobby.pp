class roles::gobby {
  include apache2
  ssl::service { 'gobby.debian.org':
    notify   => [ Exec['service apache2 reload'], Exec['reload gobby'] ],
    key      => true,
    tlsaport => [443, 6523],
  }
  file { '/etc/ssl/debian-local/other-keys/gobby.debian.org.key':
    ensure  => present,
    mode    => '0440',
    group   => 'gobby',
    content => inline_template('<%= File.read(scope().call_function("hiera", ["paths.letsencrypt_dir"]) + "/gobby.debian.org.key") %>'),
    links   => follow,
    notify  => Exec['reload gobby'],
  }
  exec { 'reload gobby':
    command     => 'pkill -u gobby -HUP -x infinoted',
    refreshonly => true,
  }

  ferm::rule::simple { 'infinoted':
    port => 6523
  }
}
