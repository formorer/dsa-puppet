#
# postgresql server role
#
# @param manage_clusters_hba  manage clusters' pg_hba.conf using postgres::cluster.  Eventually should should be true for every host and we can drop the param
# @param backups              Do backups of the database clusters on this host
# @param monitor_dbs          List of databases to monitor (default is all non-template non-default databases). Can be a boolean to indicate no / all databases
class roles::postgresql::server(
  Variant[Boolean,Array[Integer]] $manage_clusters_hba = false,
  Boolean $backups = true,
  Optional[Variant[Boolean,Array[String]]] $monitor_dbs = undef,
) {
  $clusters = $facts['postgresql_clusters']
  $clusters.each |$cluster| {
    $version      = $cluster['version']
    $cluster_name = $cluster['cluster']
    $port         = $cluster['port']

    if $manage_clusters_hba =~ Boolean {
      $hba = $manage_clusters_hba
    } else {
      $hba = $port in $manage_clusters_hba
    }
    postgres::cluster { "${version}::${cluster_name}":
      pg_version  => $version,
      pg_cluster  => $cluster_name,
      pg_port     => $port,
      manage_hba  => $hba,
      backups     => $backups,
      monitor_dbs => $monitor_dbs,
    }
  }
}
