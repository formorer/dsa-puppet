# ftp_master_dak_replica guest access to DB
#
# @param db_address       hostname of the postgres server for this service
# @param db_port          port of the postgres server for this service
# @param database         list of databases to give access to
# @param address          hosts to give access
# @param connection_type  connection type
class roles::postgresql::ftp_master_dak_replica::db_guest_access::conova (
  String  $db_address,
  Integer $db_port,
  Array[String] $database = ['projectb'],
  Enum['local', 'host', 'hostssl'] $connection_type = 'hostssl',
  Optional[Variant[Stdlib::IP::Address, Array[Stdlib::IP::Address]]] $address = $base::public_addresses,
) {
  @@postgres::cluster::hba_entry { "dak-projectb-guest-conova-${::fqdn}":
    tag             => "postgres::cluster::${db_port}::hba::${db_address}",
    pg_port         => $db_port,
    database        => $database,
    user            => 'guest',
    address         => $address,
    connection_type => $connection_type,
    method          => 'trust',
    order           => '25',
  }
}
