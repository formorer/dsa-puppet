#
# replica of the dak instance on ftp-master
#
# @param db_server  address of the database server for ftp-master's dak
# @param db_port    port of the database cluster for ftp-master's dak
# @param db_role    replication user
class roles::postgresql::ftp_master_dak_replica (
  String $db_server,
  Integer $db_port,
  String $db_role,
) {

  @@postgres::cluster::hba_entry { "dak-replica-to-${::fqdn}":
    tag      => "postgres::cluster::${db_port}::hba::${db_server}",
    pg_port  => $db_port,
    database => 'replication',
    user     => $db_role,
    address  => $base::public_addresses,
  }
}
