class roles::dedup {
  include apache2

  ssl::service { 'dedup.debian.net': notify  => Exec['service apache2 reload'], key => true, }
}
