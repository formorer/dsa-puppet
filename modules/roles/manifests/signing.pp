# code signing for EFI secure boot
class roles::signing {
  ensure_packages([
    'expect',
    'pesign',
    'linux-kbuild-4.19',
    'libengine-pkcs11-openssl',
    ], ensure => installed)
}
