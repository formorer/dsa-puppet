class roles::wiki {
  include apache2
  include roles::sso_rp

  ssl::service { 'wiki.debian.org':
    notify => Exec['service apache2 reload'],
    key    => true,
  }
  rsync::site { 'wiki':
    source => 'puppet:///modules/roles/wiki/rsyncd.conf',
  }
}
