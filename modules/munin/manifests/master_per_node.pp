# entry per-node to be collected on the master
define munin::master_per_node(
  $client_fqdn               = $name,
) {
  file { "/etc/munin/munin-conf.d/${name}.conf":
    content => template('munin/munin.conf_per_node.erb'),
  }
}
