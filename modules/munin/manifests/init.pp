# our munin class
class munin {
  package { 'munin-node':
    ensure => installed
  }

  service { 'munin-node':
    ensure  => running,
    require => Package['munin-node'],
  }

  file { '/var/log/munin':
    ensure => directory,
    owner  => root,
    group  => 'www-data',
    mode   => '0755',
  }

  file { '/etc/munin/munin-node.conf':
    content => template('munin/munin-node.conf.erb'),
    require => Package['munin-node'],
    notify  => Service['munin-node'],
  }

  file { '/etc/munin/plugin-conf.d/munin-node':
    content => template('munin/munin-node.plugin.conf.erb'),
    require => Package['munin-node'],
    notify  => Service['munin-node'],
  }

  file { '/etc/logrotate.d/munin-node':
    source  => 'puppet:///modules/munin/logrotate',
    require => Package['munin-node'],
  }

  file { '/etc/munin/plugins/df':
    ensure  => link,
    target  => '/usr/share/munin/plugins/df',
    require => Package['munin-node'],
    notify  => Service['munin-node'],
  }

  file { '/etc/munin/plugins/df_abs':
    ensure  => file,
    source  => 'puppet:///modules/munin/df-wrap',
    mode    => '0555',
    require => Package['munin-node'],
    notify  => Service['munin-node'],
  }

  file { '/etc/munin/plugins/df_inode':
    ensure  => link,
    target  => '/usr/share/munin/plugins/df_inode',
    require => Package['munin-node'],
    notify  => Service['munin-node'],
  }

  @@munin::master_per_node { $::fqdn: }

  package { 'munin-async':
    ensure => installed
  }
  service { 'munin-async':
    ensure  => running,
    require => Package['munin-async'],
  }
  dsa_systemd::override { 'munin-async':
    content  => @("EOF"),
      [Service]
      RestartSec=10
      [Unit]
      PartOf=munin-node.service
      | EOF
  }
  file { '/etc/ssh/userkeys/munin-async':
    ensure => 'absent',
  }
  ssh::authorized_key_collect { 'munin-async-fetcher':
    target_user => 'munin-async',
    collect_tag => 'munin::munin-async-fetch',
  }
}
