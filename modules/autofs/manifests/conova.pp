class autofs::conova {
	include autofs::common

	file { '/etc/auto.master.d/dsa.autofs':
		source  => "puppet:///modules/autofs/conova/auto.master.d-dsa.autofs",
		notify  => Exec['autofs reload']
	}
	file { '/etc/auto.dsa':
		source  => "puppet:///modules/autofs/conova/auto.dsa",
		notify  => Exec['autofs reload']
	}

	file { '/srv/mirrors': ensure  => directory }
	file { '/srv/mirrors/debian': ensure  => '/auto.dsa/debian' }
	file { '/srv/mirrors/debian-security': ensure  => '/auto.dsa/debian-security' }
	file { '/srv/mirrors/debian-buildd': ensure  => '/auto.dsa/debian-buildd' }
	file { '/srv/mirrors/debian-debug': ensure  => '/auto.dsa/debian-debug' }
}
