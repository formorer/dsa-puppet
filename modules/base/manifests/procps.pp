# This class defines the procps service which is notified by base::sysctl
class base::procps {
  service { 'procps':
    hasstatus => false,
    status    => '/bin/true',
  }
}
