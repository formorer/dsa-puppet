define dnsextras::dkim_record (
	$zone,
	$keyfile,
	$selector,
	$hostname,
	$domain,
) {
	$snippet = gen_dkim_entry($keyfile, $selector, $hostname, $domain)
	dnsextras::entry{ "$name":
		zone    => "$zone",
		content => $snippet,
	}
}

