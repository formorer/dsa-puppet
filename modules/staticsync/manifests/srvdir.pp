# create the directory on static hosts and disable backups
class staticsync::srvdir (
) {
  file { $staticsync::basedir:
    ensure => directory,
    mode   => '0755',
    owner  => $staticsync::user,
    group  => $staticsync::user,
  }

  file { "${staticsync::basedir}/.nobackup":
    content => '',
  }
}
