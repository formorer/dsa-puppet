# LVM config for the lenovo x86 servers that make up ganeti2.conova.debian.org
class profile::lvm::ganeti2_conova {
  class { 'lvm':
    global_filter  => '[ "a|^/dev/md[0-9]*$|", "r/.*/" ]',
    issue_discards => true,
  }
}
