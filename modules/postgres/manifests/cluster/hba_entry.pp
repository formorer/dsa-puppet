# An entry in pg_hba and the corresponding firewall rule if necessary
#
# This currently only supports a limited number of entry types.  Only
# what we need at the moment.
#
# See the upstream documentation at https://www.postgresql.org/docs/11/auth-pg-hba-conf.html
# for details.
#
# Default order is 50, postgres::cluster puts the md5 localhost rules at 30,
# so guest/trust access should probably go at 25.
#
# @param pg_port          port of the postgres cluster
# @param pg_cluster       cluster name
# @param pg_version       pg version of the cluster
# @param connection_type  connection type
# @param database         database (or all, sameuser, replication, etc.)
# @param user             user (or all, etc.)
# @param address          hosts that match
# @param method           auth method
# @param order            ordering of this entry in pg_hba.conf
# @param firewall         also add a firewall rule
define postgres::cluster::hba_entry (
  Optional[Integer] $pg_port = undef,
  Optional[String] $pg_cluster = undef,
  Optional[String] $pg_version = undef,
  Enum['local', 'host', 'hostssl'] $connection_type = 'hostssl',
  Variant[String,Array[String]] $database = 'sameuser',
  Variant[String,Array[String]] $user = 'all',
  Optional[Variant[Stdlib::IP::Address, Array[Stdlib::IP::Address]]] $address = undef,
  Enum['md5', 'trust'] $method = 'md5',
  String $order = '50',
  Boolean $firewall = true,
) {
  $address_methods = ['md5', 'trust']
  if $method in $address_methods {
    if !$address {
      fail("Authentication method ${method} needs an address")
    }
  } else {
    if !($method in $address_methods) {
      fail("Authentication method ${method} needs no address")
    }
  }

  # get remaining cluster info and verify consistency
  ###
  $clusters = $facts['postgresql_clusters']
  if $pg_port {
    $filtered = $clusters.filter |$cluster| { $cluster['port'] == $pg_port }
    if $filtered.length != 1 {
      fail("Did not find exactly one cluster with port ${pg_port}")
    }
    $cluster = $filtered[0]
  } elsif $pg_cluster and $pg_version {
    $filtered = $clusters.filter |$cluster| { $cluster['version'] == $pg_version and $cluster['cluster'] == $pg_cluster}
    if $filtered.length != 1 {
      fail("Did not find exactly one cluster ${pg_version}/${pg_cluster}")
    }
    $cluster = $filtered[0]
  } else {
    fail('postgres::cluster::hba_entry needs either the port of both a pg version and cluster name')
  }
  $real_port    = $cluster['port']
  $real_version = $cluster['version']
  $real_cluster = $cluster['cluster']
  if $pg_version and $pg_version != $real_version {
    fail("Inconsisten cluster version information: ${pg_version} != ${real_version}")
  }
  if $pg_cluster and $pg_cluster != $real_cluster {
    fail("Inconsisten cluster name information: ${pg_cluster} != ${real_cluster}")
  }
  ###

  if ($address and $firewall) {
    ferm::rule::simple { "postgres::cluster::hba_entry::${name}":
      description => "allow access to pg${real_version}/${real_cluster}: ${name}",
      saddr       => $address,
      chain       => "pg-${real_port}",
    }
  }

  $real_database = Array($database, true).sort().join(',')
  $real_user     = Array($user, true).sort().join(',')
  $real_address  = $address ? {
    undef   => [''],
    default => Array($address, true).map |$a| {
      if    $a =~ Stdlib::IP::Address::V4::CIDR     { $a }
      elsif $a =~ Stdlib::IP::Address::V4::Nosubnet { "${a}/32" }
      elsif $a =~ Stdlib::IP::Address::V6::CIDR     { $a }
      elsif $a =~ Stdlib::IP::Address::V6::Nosubnet { "${a}/128" }
      else { fail("Do not know address type for ${a}") }
    }
  }

  @concat::fragment { "postgres::cluster::pg_hba::${name}":
    tag     => "postgres::cluster::${real_version}::${real_cluster}::hba",
    target  => "postgres::cluster::${real_version}::${real_cluster}::hba",
    order   => $order,
    content => inline_template( @(EOF) ),
                  #
                  # rule <%= @name %>
                  <% @real_address.each do |addr| -%>
                  <%= [@connection_type, @real_database, @real_user, addr, @method].join(' ') %>
                  <% end -%>
                  #
                  | EOF
  }
}
