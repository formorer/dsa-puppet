#! /bin/bash
#
# Force a checkpoint to occur on any PostgreSQL (>=10) cluster that has
# not output WAL logs within a given time period
#
# Between versions 9.4 and 9.6, WAL logs were generated at regular
# intervals whether any activity occurred or not. This was reverted to the
# pre-9.4 behaviour of only producing logs when there was activity as part
# of https://commitfest.postgresql.org/12/628/
set -e
set -u

minutes=$((5*60))

pg_lsclusters -h | while read version cluster port status owner datadir logfile; do
  echo "$status" | grep -qw online || continue
  version=$(awk "BEGIN{print $version * 100}")
  [ $version -ge 1000 ] || continue
  files="$(find ${datadir}/pg_wal -type f -cmin -$minutes)"
  [ -n "$files" ] || psql -p $port -c 'CHECKPOINT'
done

