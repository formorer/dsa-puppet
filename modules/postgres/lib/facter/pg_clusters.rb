# Get an array of all postgresql clusters on this system
# using pg_lsclusters.
#
# Copyright 2019, Peter Palfrader
#
Facter.add(:postgresql_clusters) do
  setcode do
    require 'tmpdir'
    dblist_q = %{
                  SELECT datname,
                         pg_catalog.pg_get_userbyid(datdba),
                         pg_catalog.pg_encoding_to_char(encoding),
                         datcollate,
                         datctype,
                         datistemplate,
                         datallowconn
                  FROM   pg_database
    }

    clusters = []
    begin
      IO.popen(['pg_lsclusters', '-h']) do |f|
        f.each_line do |line|
          (version, cluster, port, status, owner, datadir, logfile, _) = line.split()
          databases = []
          if status.split(',').include?('online')
            begin
              IO.popen(['sudo', '-u', 'postgres', 'psql', '-p', port, '-tAX', '-c', dblist_q],
                       :chdir => Dir.tmpdir) do |d|
                d.each_line do |dbline|
                  (database, owner, encoding, collation, ctype, istemplate, allowconn, _) = dbline.chomp().split('|')
                  databases << {
                    'name' => database,
                    'owner' => owner,
                    'encoding' => encoding,
                    'collation' => collation,
                    'ctype' => ctype,
                    'istemplate' => case istemplate when 't' then true else false end,
                    'allowconn' => case allowconn when 't' then true else false end,
                  }
                end
              end
            rescue Errno::ENOENT
            end
          end
          clusters << {
            'version' => version,
            'cluster' => cluster,
            'port' => port.to_i(),
            'status' => status.split(',').product([true]).to_h(),
            'owner' => owner,
            'datadir' => datadir,
            'logfile' => logfile,
            'databases' => databases,
          }
        end
      end
    rescue Errno::ENOENT
    end
    clusters
  end
end
